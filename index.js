const express = require("express")
const dotenv = require("dotenv")
const mongoose = require("mongoose")
const cors = require ("cors")
const userRoutes = require("./routes/userRoutes")
const productRoutes = require("./routes/productRoutes")
const orderRoutes = require("./routes/orderRoutes")


dotenv.config()

const app = express()
const port = process.env.PORT || 3001

// MongoDB CONNECTION----------------------

mongoose.connect(`mongodb+srv://yovrey:${process.env.MONGODB_PASSWORD}@cluster0.rrk947v.mongodb.net/capstone2-ecommerce-api?retryWrites=true&w=majority`, {
	useNewUrlParser: true,
	useUnifiedTopology: true
})

let db = mongoose.connection
db.once("open", () => console.log("Connected to MongoDB!"))

// MongoDB CONNECTION END----------------------


//To avoid CORS errors when trying to send request to our server----------
app.use(cors())
app.use(express.json())
app.use(express.urlencoded({extended: true}))

// ROUTES--------------------------------------
app.use("/users", userRoutes)
app.use("/products", productRoutes)
app.use("/orders", orderRoutes)

// ROUTES END----------------------------------



app.listen(port, () => {
	console.log(`API is now running on localhost:${port}`)
})