const User = require("../models/User")
const Order = require ("../models/Order")
const Product = require ("../models/Product")

const bcrypt = require("bcrypt")
const auth = require("../auth.js")



// Check if email exist
module.exports.checkIfEmailExists = (data) => {
	return User.find({email: data.email}).then((result) => {
		if(result.length > 0) {
			return {message: "Email already exist!"}
		}

		return {message: "Email does not exist!"}
	})
}



// User registration
module.exports.register = (data) => {

	let encrypted_password = bcrypt.hashSync(data.password, 10)

	let new_user = new User({
		email: data.email,
		password: encrypted_password
	})

	return new_user.save().then((created_user, error) => {
		if(error) {
			return false
		}

		return {
			message: "You have successfully registered. Welcome to our store!"
		}
	})
}



// User login/authentication
module.exports.login = (data) => {
	return User.findOne({email: data.email}).then((result) => {
		if(result == null) {
			return {
				messageToken: "User doesn't exist!"
			}
		}

		const is_password_correct = bcrypt.compareSync(data.password, result.password)

		if(is_password_correct) {
			return { access: auth.createAccessToken(result) }
		}

		return { message: "Password is incorrect!" }
	})
}



// Get single user datails
module.exports.getUserDetails = (userId) => {
	return User.findById(userId, {password: 0}).then((result, error) => {
		if(error) {
			return false
		}
		else {
			return result
		}
	})
}



// Set user as Admin ---- ADMIN ONLY
module.exports.makeAdmin = async (user, request) => {

	if(user.isAdmin){	

		return User.findByIdAndUpdate(request.userId, { isAdmin: true }).then((user, error) => {
			if(error){
				return false;
			}
			else{
				return ('User is now an admin!')
			}
		})
	}
	else{
		return (`You have no access!`)
	}
}


