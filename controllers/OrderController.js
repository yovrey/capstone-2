const User = require("../models/User")
const Order = require ("../models/Order")
const Product = require ("../models/Product")

const bcrypt = require("bcrypt")
const auth = require("../auth.js")


// Create order -----NON ADMIN ONLY
module.exports.checkout = (data) => {
	if(data.isAdmin) {
		return Promise.resolve({
			message: "You are no allowed to create an order!"
		})
	}

	return Product.findById(data.order.productId).then((result) => {
		if(result.isActive) {
			let new_order = new Order({
				userId: data.userId,
				products: {
					productId: data.order.productId,
					quantity: data.order.quantity
				},
				totalAmount: result.price*data.order.quantity,
				purchasedOn: data.order.purchasedOn
			})
			return new_order.save().then((new_order, error) => {
				if (error) {
					return false
				}
				return {
					message: "New order has been created successfully!"
				}
			})
		}
		return Promise.resolve({
			message: "Product is currently unavailable."
		})
	}).catch((error) => {return {message: "Product is not available!"}})
}

// Get all orders -----ADMIN ONLY
module.exports.getOrders = async (user) => {

	if(user.isAdmin) {
		return Order.find({}).then((result) => {
			return result
			})
	}
	return {message: "You have no access!"}
}












