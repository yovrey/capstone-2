const User = require("../models/User")
const Order = require ("../models/Order")
const Product = require ("../models/Product")


// CREATE A PRODUCT ---- ADMIN ONLY
module.exports.createProduct = async (user, reqBody) => {

	if(user.isAdmin){	
		let newProduct = new Product ({
			name : reqBody.name,
			description : reqBody.description,
			price : reqBody.price
		})
		return newProduct.save().then((product, error) => {
			if(error){
				return false;
			}
			else{
				return ('New product created successfully!');
			}
		})
	}
	else{
		return ('You have no access!')
	}
}



// GET PRODUCT DETAILS
module.exports.getProductDetails = (productId) => {
	return Product.findById(productId).then((result, error) => {
		if(error) {
			return false
		}
		else {
			return result
		}
	})
}



// GET ALL ACTIVE PRODUCTS
module.exports.getAllActive = () => {
	return Product.find({isActive: true}).then((result) => {
		return result
	})
}



// UPDATE A SINGLE PRODUCT -----ADMIN ONLY
module.exports.updateProduct = async (user, reqParams, reqBody) => {
	
	if(user.isAdmin){
	
		let updatedProduct = {
			name : reqBody.name,
			description : reqBody.description,
			price : reqBody.price
		}

		return Product.findByIdAndUpdate(reqParams.productId, updatedProduct).then((product, error) => {
			if(error){
				return false;
			}
			else{
				return ('The product has been updated!');
			}
		})
	}else{
		return (`You have no access!`);
	}
}



// ARCHIVING A PRODUCT -----ADMIN ONLY
module.exports.archiveProduct = async (user, reqParams, reqBody) => {

	if(user.isAdmin){
	
		let archivedProduct = {
			isActive : false	
		}

		return Product.findByIdAndUpdate(reqParams.productId, archivedProduct).then((product, error) => {
			if(error){
				return false;
			}
			else{
				return ('The product has been archived!');
			}
		})
	}else{
		return (`You have no access!`);
	}
}

