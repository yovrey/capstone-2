const express = require("express")
const router = express.Router()

const OrderController = require("../controllers/OrderController")
const auth = require("../auth")


// Create order -----NON ADMIN ONLY
router.post("/checkout", auth.verify, (request, response) => {
	const user = auth.decode(request.headers.authorization)
	const data = {
		order: request.body,
		userId: user.id,
		isAdmin: user.isAdmin
	}
	OrderController.checkout(data).then((result) => {
		response.send(result)
	})
})





// Get all orders -----ADMIN ONLY
router.get("/all-orders", auth.verify, (request, response) => {
	const user = auth.decode(request.headers.authorization)
	OrderController.getOrders(user).then((result) => {
		response.send(result)
	})
})




  



module.exports = router 