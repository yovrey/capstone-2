
const express = require("express")
const router = express.Router()

const UserController = require("../controllers/UserController")
const auth = require("../auth")

// Check if email already exist in the database
router.post("/check-email", (request, response) => {
	UserController.checkIfEmailExists(request.body).then((result) => {
		response.send(result)
	})
})


// Register a user
router.post("/register", (request, response) => {
	UserController.register(request.body).then((result) => {
		response.send(result)
	})
})


// User Authentication
router.post("/login", (request, response) => {
	UserController.login(request.body).then((result) => {
		response.send(result)
	})
})


// Retrieve single user data
router.get("/:id/user-details", auth.verify, (request, response) => {
	UserController.getUserDetails(request.params.id).then((result) => {
		response.send(result)
	})
})


// Set user as Admin ------- yhong@email.com, admin123 
router.patch("/:userId/make-admin", auth.verify, (request, response) => {
	const user = auth.decode(request.headers.authorization)
	UserController.makeAdmin(user, request.params, request.body).then((result) => {
		response.send(result)
	})
})




module.exports = router
