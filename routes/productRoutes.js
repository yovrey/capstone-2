const express = require("express")
const router = express.Router()

const ProductController = require("../controllers/ProductController")
const auth = require("../auth")


// Create a product -----ADMIN ONLY
router.post("/create-product", auth.verify, (request, response) => {
	const user = auth.decode(request.headers.authorization)
	ProductController.createProduct(user, request.body).then(result => response.send(result));
})





// Get single product details
router.get("/:id/product-details", auth.verify, (request, response) => {
	ProductController.getProductDetails(request.params.id).then((result) => {
		response.send(result)
	})
})




// Get all active products
router.get("/active", auth.verify, (request, response) => {
	ProductController.getAllActive().then((result) => {
		response.send(result)
	})
})




// Update a product details -----ADMIN ONLY
router.patch("/:productId/update", auth.verify, (request, response) => {
	const user = auth.decode(request.headers.authorization)
	ProductController.updateProduct(user, request.params, request.body).then((result) => {
		response.send(result)
	})
})




// Archiving a single product -----ADMIN ONLY
router.patch("/:productId/archive", auth.verify, (request, response) => {
	const user = auth.decode(request.headers.authorization)
	ProductController.archiveProduct(user, request.params, request.body).then((result) => response.send(result));
})




module.exports = router